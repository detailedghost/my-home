import { useState } from "react";
import { useEffect } from "react";
import { Random } from "unsplash-js/dist/methods/photos/types";
// import { UnsplashApi } from "../../api/Unsplash";
import BackgroundImageContainer from "./BackgroundImageContainer";
import { BackgroundContainerAttr } from "./container.models";

const CACHE_KEY = "bck-img-cache";

const RandomBackgroundImageContainer = (
	props: Pick<BackgroundContainerAttr, "width" | "height" | "children">
) => {
	const [photo, setPhoto] = useState<Random | undefined>(undefined);
	useEffect(() => {
		const cachedItem = localStorage.getItem(CACHE_KEY);
		if (cachedItem) {
			const cachedPhoto = JSON.parse(cachedItem);
			if (Date.parse(cachedPhoto.exp) > Date.now()) {
				setPhoto(JSON.parse(cachedItem));
				return;
			}
		}

		// UnsplashApi.photos
		// 	.getRandom({ orientation: "landscape", count: 1 })
		// 	.then(({ response }) => {
		// 		if (response) {
		// 			setPhoto(response[0]);
		// 			localStorage.setItem(
		// 				CACHE_KEY,
		// 				JSON.stringify({
		// 					...response[0],
		// 					exp: new Date(Date.now() + 1000 * 60 * 60 * 24),
		// 				})
		// 			);
		// 		}
		// 	});
	}, []);

	return (
		<BackgroundImageContainer
			width={props.width}
			height={props.height}
			imgUrl={photo?.urls?.full ?? ""}
		>
			{props.children}
			{/* {() =>
				photo ? (
					<aside>
						Photo by{" "}
						<a href="https://unsplash.com/@anniespratt?utm_source=my-home&utm_medium=referral">
							{photo?.user.first_name} {photo?.user.last_name}
						</a>{" "}
						on
						<a href="https://unsplash.com/?utm_source=my-home&utm_medium=referral">
							Unsplash
						</a>
					</aside>
				) : (
					<></>
				)
			} */}
		</BackgroundImageContainer>
	);
};

export default RandomBackgroundImageContainer;
