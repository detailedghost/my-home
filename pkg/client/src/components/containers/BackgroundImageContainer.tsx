import { ReactNode } from "react";
import { BackgroundContainerAttr } from "./container.models";
import styles from "./container.module.css";

export default function BackgroundImageContainer({
	width,
	height,
	imgUrl,
	children,
}: BackgroundContainerAttr & { imgUrl: string }) {
	return (
		<section
			className={styles.backgroundImageContainer}
			style={{
				width: width,
				height: height,
				backgroundImage: `url(${imgUrl})`,
			}}
		>
			{children}
		</section>
	);
}
