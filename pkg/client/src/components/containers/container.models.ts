import { ReactNode } from "react";

export interface BackgroundContainerAttr {
	width?: string;
	height?: string;
	children?: ReactNode;
}
