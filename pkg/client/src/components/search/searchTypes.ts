export const ALL_VALID_SEARCH_PROVIDERS = [
  "google",
  "bing",
  "yandex",
  "yahoo",
  "duckduckgo",
  "default",
] as const;

export type ValidSearchProvider =
  | "google"
  | "bing"
  | "yandex"
  | "yahoo"
  | "duckduckgo"
  | "default";

const execSearch = (providerFn: (query: string) => string) => (
  query: string,
  newTab: boolean = false
) =>
  newTab
    ? window.open(providerFn(encodeURIComponent(query).toString()), "_blank")
    : (location.href = providerFn(encodeURIComponent(query).toString()));

export const SearchProviderExecSearch: Record<
  ValidSearchProvider,
  (query: string) => void
> = {
  google: execSearch((q) => `https://google.com/search?q=${q}`),
  bing: execSearch((q) => `https://bing.com?q=${q}`),
  yandex: execSearch((q) => `https://yandex.com?q=${q}`),
  yahoo: execSearch((q) => `https://yahoo.com/search?q=${q}`),
  duckduckgo: execSearch((q) => `https://ddg.gg?q=${q}`),
  default: execSearch((q) => `#/?q=${q}`),
};
