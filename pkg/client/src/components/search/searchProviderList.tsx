import SearchProvider from "./searchProvider";
import { ALL_VALID_SEARCH_PROVIDERS, ValidSearchProvider } from "./searchTypes";
import styles from "./searchBar.module.css";
import { useCallback, useState } from "react";

interface SearchProviderListProps {
  currentProvider: ValidSearchProvider;
  className?: string;
  onChange?: (p: ValidSearchProvider) => void;
}

export default function SearchProviderList(props: SearchProviderListProps) {
  const [search, setSearch] = useState<boolean>(false);

  const onClickCurrentCb = useCallback((_) => setSearch(!search), [search]);

  const onClickItemCb = (p: ValidSearchProvider) => {
    if (props.onChange) props?.onChange(p);
    setSearch(!search);
  };

  return (
    <>
      <span onClick={onClickCurrentCb}>
        <SearchProvider
          provider={props.currentProvider}
          className={props.className}
        ></SearchProvider>
      </span>
      <div
        className={
          search
            ? styles.searchProviderList + " " + styles.show
            : styles.searchProviderList
        }
      >
        <ul>
          {ALL_VALID_SEARCH_PROVIDERS.filter((i) => i !== "default").map(
            (p, i) => (
              <li key={i} onClick={() => onClickItemCb(p)}>
                <SearchProvider provider={p} />
              </li>
            )
          )}
        </ul>
      </div>
    </>
  );
}
