import {
  faGoogle,
  faMicrosoft,
  faYahoo,
  faYandex,
  IconDefinition,
} from "@fortawesome/free-brands-svg-icons";
import { faBox, faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ValidSearchProvider } from "./searchTypes";

interface SearchProviderProps {
  provider?: ValidSearchProvider;
  className?: string;
}

export default function SearchProvider({
  provider,
  className,
}: SearchProviderProps) {
  const iconMap: Record<ValidSearchProvider, IconDefinition> = {
    google: faGoogle,
    bing: faMicrosoft,
    yandex: faYandex,
    yahoo: faYahoo,
    duckduckgo: faUserSecret,
    default: faBox,
  };

  return (
    <>
      {Object.keys(iconMap).some((v) => v === provider) ? (
        <FontAwesomeIcon
          className={className}
          icon={iconMap[provider || "default"]}
        ></FontAwesomeIcon>
      ) : (
        <FontAwesomeIcon
          className={className}
          icon={iconMap.default}
        ></FontAwesomeIcon>
      )}
    </>
  );
}
