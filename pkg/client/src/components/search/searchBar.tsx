import SearchProvider from "./searchProvider";
import styles from "./searchBar.module.css";
import { SearchProviderExecSearch, ValidSearchProvider } from "./searchTypes";
import { useCallback, useEffect, useReducer } from "react";
import { capitalize } from "lodash";
import SearchProviderList from "./searchProviderList";
import Cookie from "js-cookie";

const cookieProviderKey = "search:provider";
const cookieQueryKey = "search:query";

const generatePlaceholderText = (text: ValidSearchProvider) =>
  `Search ${text === "default" ? "" : "via " + capitalize(text)}...`;

interface SearchBarState {
  provider: ValidSearchProvider;
  query: string;
}
const reducer = (
  state: SearchBarState,
  action: { type: string; payload: any }
) => {
  switch (action.type) {
    case "provider:u":
      Cookie.set(cookieProviderKey, action.payload, { expires: 365 });
      return {
        ...state,
        provider: action.payload,
      };

    case "query:u":
      return {
        ...state,
        query: action.payload,
      };

    default:
      return { ...state };
  }
};

const SearchBar = () => {
  const [state, dispatch] = useReducer(reducer, {
    provider: Cookie.get(cookieProviderKey) || "google",
    query: "",
  });

  const onSearchKeyUp = useCallback(
    (e) => {
      dispatch({ type: "query:u", payload: e.target.value });
      if (e.keyCode === 13) {
        return SearchProviderExecSearch[state.provider](state.query, false);
      }
    },
    [state.query]
  );

  const inputPHText = generatePlaceholderText(state.provider);

  return (
    <div className={styles.section}>
      <div>
        <SearchProviderList
          currentProvider={state.provider}
          className={styles.searchIcon}
          onChange={(payload: ValidSearchProvider) =>
            dispatch({ type: "provider:u", payload })
          }
        />
        <input
          placeholder={inputPHText}
          alt={inputPHText}
          onKeyUp={onSearchKeyUp}
        />
      </div>
    </div>
  );
};

export default SearchBar;
