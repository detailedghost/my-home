import SearchBar from "./search/searchBar";
import styles from "./app.module.css";
import RandomBackgroundImageContainer from "./containers/RandomBackgroundImageContainer";

export function App() {
	// https://api.weather.gov/points/{latitude},{longitude}
	// https://wallhaven.cc/api/v1/search
	return (
		<main className={styles.universe}>
			<RandomBackgroundImageContainer width="100%" height="100%">
				<div className={styles.searchBarContainer}>
					<SearchBar></SearchBar>
				</div>
			</RandomBackgroundImageContainer>
		</main>
	);
}
