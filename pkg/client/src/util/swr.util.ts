export const fetcher = (request: RequestInfo, config?: RequestInit) =>
  fetch(request, config).then((res) => res.json());
