import Fastify, { FastifyInstance, RouteShorthandOptions } from "fastify";
const PORT = process.env.PORT ?? 3001;

const defaultOpts: RouteShorthandOptions = {
	schema: {
		response: {
			200: {
				type: "object",
				properties: {
					pong: {
						type: "string",
					},
				},
			},
		},
	},
};

const server: FastifyInstance = Fastify({});

server.get("/ping", defaultOpts, async (req, res) => {
	return {
		pong: "it worked",
	};
});

const start = async () => {
	try {
		await server.listen(PORT);

		const address = server.server.address();
		const port = typeof address === "string" ? address : address?.port;
	} catch (err) {
		server.log.error(err);
		process.exit(1);
	}
};

try {
	start();
} catch (error) {
	server.log.error(error);
}
